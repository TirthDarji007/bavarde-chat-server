#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "csapp.h"
#include "debug.h"
#include "server.h"
#include "directory.h"
#include "thread_counter.h"
#include "mytime.h"

static void terminate();

void parse_args(int argc, char *argv[]);

THREAD_COUNTER *thread_counter;
int port = 0;
int listenfd, *connfdp;

int main(int argc, char* argv[]) {
    // Option processing should be performed here.
    // Option '-p <port>' is required in order to specify the port number
    // on which the server should listen.
    parse_args(argc, argv);
    debug("port is %d", port);

    // Perform required initializations of the thread counter and directory.
    thread_counter = tcnt_init();
    dir_init();
    mytime_init();

    // TODO: Set up the server socket and enter a loop to accept connections
    // on this socket.  For each connection, a thread should be started to
    // run function bvd_client_service().  In addition, you should install
    // a SIGHUP handler, so that receipt of SIGHUP will perform a clean
    // shutdown of the server.
    Signal(SIGHUP, terminate); //csapp library
    Signal(SIGTERM, terminate);
    Signal(SIGINT, terminate);
    // Signal(SIGKILL, terminate);

    //set up listning of port
    socklen_t clientlen;
    struct sockaddr_storage clientaddr;
    pthread_t tid;
    if (port < 1024) {
        port = 11000;
        debug("port changed to default value of 11000.");
        printf("port changed to default value of 11000.\n");
    }
    listenfd = Open_listenfd(port);
    while (1) {
        clientlen = sizeof(struct sockaddr_storage);
        connfdp = Malloc(sizeof(int));
        *connfdp = Accept(listenfd, (SA *) &clientaddr, &clientlen);
        Pthread_create(&tid, NULL, bvd_client_service, connfdp);
        debug("new thread %lu has been created.", tid);
    }

    fprintf(stderr, "You have to finish implementing main() "
            "before the Bavarde server will function.\n");

    terminate();
}

/*
 * Function called to cleanly shut down the server.
 */
void terminate(int sig) {
    //Free the stuff and stop listing to port
    Free(connfdp);
    Close(listenfd);
    // Shut down the directory.
    // This will trigger the eventual termination of service threads.
    dir_shutdown();

    debug("Waiting for service threads to terminate...");
    tcnt_wait_for_zero(thread_counter);
    debug("All service threads terminated.");

    tcnt_fini(thread_counter);
    dir_fini();

    close(0);
    close(1);
    close(2);
    exit(EXIT_SUCCESS);
}

//Helper Functions
void parse_args(int argc, char *argv[]) {
    char option;
    while ((option = getopt(argc, argv, "+p:")) != -1) {
        switch (option) {
        case 'p': {
            port = atoi(optarg);
            break;
        }
        default: {
            break;
        }
        }
    }
}
