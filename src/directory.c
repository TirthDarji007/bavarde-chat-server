#include "directory.h"
#include "csapp.h"
#include "debug.h"

//directory structures
typedef struct directory {
    MAILBOX *mailbox;
    int sockfd;
    struct directory *next;
    struct directory *prev;
} DIRECTORY;

//Local Variables
static DIRECTORY *head, *tail;
static sem_t mutex; //list manupulation mutex
static sem_t ar_mutex; //add and remove dir mutex
volatile static int members = 0; //Total number of directories. Must be volatile

//Helpers
void remove_list(DIRECTORY *dir);
void insert_list(DIRECTORY *dir);
DIRECTORY *find_dir(char *handle);

/*
 * Initialize the directory.
 */
void dir_init(void) {
    debug("dir_init called");
    head = tail  = NULL;
    Sem_init(&mutex, 0, 1);
    Sem_init(&ar_mutex, 0, 1);
    members = 0;
    //malloc will be done by the dir_register
    //free will be done by the remove_list function
}

/*
 * Shut down the directory.
 * This marks the directory as "defunct" and shuts down all the client sockets,
 * which triggers the eventual termination of all the server threads.
 */
void dir_shutdown(void) {
    debug("dir_shutdown called");
    P(&mutex);
    P(&ar_mutex);
    DIRECTORY *cursor = head;
    while (cursor != NULL) {
        shutdown(cursor->sockfd, SHUT_RDWR);
        mb_shutdown(cursor->mailbox);
        cursor = cursor->next;
    }
    //all things should never be unloack since it is being shutdown
    V(&mutex);
    V(&ar_mutex);
}

/*
 * Finalize the directory.
 *
 * Precondition: the directory must previously have been shut down
 * by a call to dir_shutdown().
 */
void dir_fini(void) {
    debug("dir_fini called");
    P(&mutex);
    P(&ar_mutex);
    DIRECTORY *cursor = head;
    DIRECTORY *temp;
    while (cursor != NULL) {
        temp = cursor->next;
        remove_list(cursor);
        cursor = temp;
    }
    V(&mutex);
    V(&ar_mutex);
}

/*
 * Register a handle in the directory.
 *   handle - the handle to register
 *   sockfd - file descriptor of client socket
 *
 * Returns a new mailbox, if handle was not previously registered.
 * Returns NULL if handle was already registered or if the directory is defunct.
 */
MAILBOX *dir_register(char *handle, int sockfd) {
    debug("dir_register called");
    P(&ar_mutex);
    debug("dir_register got ar_mutex");
    MAILBOX *mb = dir_lookup(handle);
    if (mb != NULL) {
        mb_unref(mb);
        debug("dir_register release ar_mutex");
        V(&ar_mutex);
        return NULL;
    }
    mb = mb_init(handle);
    DIRECTORY *dir = Calloc(1, sizeof(DIRECTORY));
    dir->sockfd = sockfd;
    dir->mailbox = mb;
    insert_list(dir);
    mb_ref(mb);
    debug("dir_register release ar_mutex");
    V(&ar_mutex);
    return (dir->mailbox);
}

/*
 * Unregister a handle in the directory.
 * The associated mailbox is removed from the directory and shut down.
 */
void dir_unregister(char *handle) {
    debug("dir_unregister called");
    P(&ar_mutex);
    DIRECTORY *dir =find_dir(handle);
    MAILBOX *mb = dir->mailbox;
    if ( mb == NULL ) {V(&ar_mutex);return; }//no handle found
    shutdown(dir->sockfd, SHUT_RDWR);
    mb_shutdown(dir->mailbox);
    mb_unref(mb);
    remove_list(dir);
    V(&ar_mutex);
}

/*
 * Query the directory for a specified handle.
 * If the handle is not registered, NULL is returned.
 * If the handle is registered, the corresponding mailbox is returned.
 * The reference count of the mailbox is increased to account for the
 * pointer that is being returned.  It is the caller's responsibility
 * to decrease the reference count when the pointer is ultimately discarded.
 */
MAILBOX *dir_lookup(char *handle) {
    debug("my dir_lookup called");
    P(&mutex);
    DIRECTORY *cursor = head;
    while (cursor != NULL) {
        if (!strcmp(mb_get_handle(cursor->mailbox), handle)) {
            mb_ref(cursor->mailbox);
            V(&mutex);
            return cursor->mailbox;
        }
        cursor = cursor->next;
    }
    V(&mutex);
    //If matched, function should have been returned by now
    return NULL;
}

/*
 * Obtain a list of all handles currently registered in the directory.
 * Returns a NULL-terminated array of strings.
 * It is the caller's responsibility to free the array and all the strings
 * that it contains.
 */
char **dir_all_handles(void){
    debug("dir_all_handles called");
    P(&mutex);
    char **ans = Calloc(members+1,sizeof(char*));
    char *handle;
    DIRECTORY *cursor = head;
    int i =0;
    while(cursor!=NULL && i<members){
        handle = mb_get_handle(cursor->mailbox);
        ans[i] = Malloc(strlen(handle) + 1);
        strcpy(ans[i],handle);
        cursor=cursor->next;
        i++;
    }
    ans[members] = NULL; //NULL teminator at last  array
    V(&mutex);
    return ans;
}



void insert_list(DIRECTORY *dir) {
    debug("insert_list called");
    P(&mutex);
    if ( head == NULL) {
        //First element
        head = tail = dir;
        members = 1;
        V(&mutex);
        return;
    }
    tail->next = dir;
    dir->prev = tail;
    tail = dir;
    members++;
    V(&mutex);
}

//This function frees the dir
void remove_list(DIRECTORY *dir) {
    debug("remove_list called");
    if (head == NULL) return;
    P(&mutex);
    if (dir == head) {
        head = head->next;
        if (head == NULL) tail = head;
        else head->prev = NULL;
    } else if (dir == tail) {
        tail = tail->prev;
        if (NULL == tail) head = tail;
        else tail->next = NULL;
    } else {
        dir->prev->next = dir->next;
        dir->next->prev = dir->prev;
        dir->next = dir->prev = NULL;
    }
    members--;
    Free(dir);
    if (members < 0) members = 0;
    V(&mutex);
}

DIRECTORY *find_dir(char *handle) {
    debug("my find_dir called");
    P(&mutex);
    DIRECTORY *cursor = head;
    while (cursor != NULL) {
        if (!strcmp(mb_get_handle(cursor->mailbox), handle)) {
            V(&mutex);
            return cursor;
        }
        cursor = cursor->next;
    }
    V(&mutex);
    //If matched, function should have been returned by now
    return NULL;
}