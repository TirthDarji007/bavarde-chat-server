#include "mailbox.h"
#include "csapp.h"
#include "debug.h"

//structure for mail - received messages. Its a queue using linked list
typedef struct mail {
    MAILBOX_ENTRY *me;
    struct mail *next;
} MAIL;

struct mailbox {
    sem_t mutex; //mutex for access
    sem_t items_mutex; //mutex of total mailbox entry
    char *handle;
    int items; // messages in mailbox
    int rcount; //reference counter
    int defunct; //defunct status of mailbox. 0 means not debuct and rest value means defunct
    MAIL *head; //head of mails
    MAIL *tail;  //tail of mails
    void (*hook)(MAILBOX_ENTRY *);
};

//Helper functions
void mb_finit(MAILBOX *mb);


/*
 * Create a new mailbox for a given handle.
 * The mailbox is returned with a reference count of 1.
 */
MAILBOX *mb_init(char *handle) {
    debug("mb_init called");
    MAILBOX *mb = Calloc(1, sizeof(MAILBOX));
    Sem_init(&mb->mutex, 0, 1);
    Sem_init(&mb->items_mutex, 0, 0); //as the item will added this will be incresed
    int len = strlen(handle) + 1;
    mb->handle = Calloc(len, sizeof(char));
    strcpy(mb->handle, handle);
    mb->items = 0;
    mb->rcount = 1;
    mb->defunct = 0;
    mb->head = NULL;
    mb->tail = NULL;
    mb->hook = NULL;
    return mb;
}

//This function free the mailbox
void mb_finit(MAILBOX *mb) {
    debug("mb_finit called");
    if (mb == NULL) return;
    if (mb->rcount != 0) debug("mb_finit called without reference going to zero");
    MAILBOX_ENTRY *me = NULL;
    //generate bounce notice
    if (mb->hook != NULL && (mb->items > 0)) {
        debug("hook function is beaing called");
        while( (mb->items>0)  && ((me=mb_next_entry(mb)) != NULL)  ){
            mb->hook(me);
        }
    }
    Free(mb->handle);
    Free(mb);
}

/*
 * Set the discard hook for a mailbox.
 */
void mb_set_discard_hook(MAILBOX *mb, MAILBOX_DISCARD_HOOK *hook) {
    debug("mb_set_discard_hook called - nothing is being done.");
    P(&mb->mutex);
    mb->hook = (void*)hook;
    V(&mb->mutex);
}


/*
 * Increase the reference count on a mailbox.
 * This must be called whenever a pointer to a mailbox is copied,
 * so that the reference count always matches the number of pointers
 * that exist to the mailbox.
 */
void mb_ref(MAILBOX *mb) {
    debug("mb_ref called");
    if (mb == NULL) return;
    P(&mb->mutex);
    mb->rcount++;
    debug(" %s reference %d->%d", mb->handle, (mb->rcount - 1), mb->rcount);
    V(&mb->mutex);
}

/*
 * Decrease the reference count on a mailbox.
 * This must be called whenever a pointer to a mailbox is discarded,
 * so that the reference count always matches the number of pointers
 * that exist to the mailbox.  When the reference count reaches zero,
 * the mailbox will be finalized.
 */
void mb_unref(MAILBOX *mb) {
    debug("mb_unref called");
    if (mb == NULL) return;
    P(&mb->mutex);
    mb->rcount--;
    debug(" %s reference %d->%d", mb->handle, (mb->rcount + 1), mb->rcount);
    if (mb->rcount == 0) {
        V(&mb->mutex);
        mb_finit(mb);
        return;
    }
    if (mb->rcount < 0)debug("reference count has error. Value is %d", mb->rcount);
    V(&mb->mutex);
}

/*
 * Shut down this mailbox.
 * The mailbox is set to the "defunct" state.  A defunct mailbox should
 * not be used to send any more messages or notices, but it continues
 * to exist until the last outstanding reference to it has been
 * discarded.  At that point, the mailbox will be finalized, and any
 * entries that remain in it will be discarded.
 */
void mb_shutdown(MAILBOX *mb) {
    //make mailbox defunct
    //release next_entry blocked thread
    //send the bouce notice if there is a hoock function - done while finalizing the mailbox.
    debug("mb_shutdown called");
    if (mb == NULL) return;
    P(&mb->mutex);
    mb->defunct = 1;
    V(&mb->items_mutex); //release next_mailbox waiting thread
    V(&mb->mutex);
}

/*
 * Get the handle associated with a mailbox.
 */
char *mb_get_handle(MAILBOX *mb) {
    debug("mb_get_handle called");
    // if(mb->defunct) return NULL;
    if (mb == NULL) return NULL;
    P(&mb->mutex);
    char *ptr = mb->handle;
    V(&mb->mutex);
    return ptr; //ptr is varible which points to handle.
}

/*
 * Add a message to the end of the mailbox queue.
 *   msgid - the message ID
 *   from - the sender's mailbox
 *   body - the body of the message, which can be arbitrary data, or NULL
 *   length - number of bytes of data in the body
 *
 * The message body must have been allocated on the heap,
 * but the caller is relieved of the responsibility of ultimately
 * freeing this storage, as it will become the responsibility of
 * whomever removes this message from the mailbox.
 *
 * The reference to the sender's mailbox ("from") is conceptually
 * "transferred" from the caller to the new message, so no increase in
 * the reference count is performed.  However, after the call the
 * caller must discard this pointer which it no longer "owns".
 */
void mb_add_message(MAILBOX *mb, int msgid, MAILBOX *from, void *body, int length) {
    debug("mb_add_message called");
    if (mb->defunct) return;
    P(&mb->mutex);
    if (mb->defunct) {V(&mb->mutex); return;}
    MAILBOX_ENTRY *me = Calloc(1, sizeof(MAILBOX_ENTRY));
    me->type = MESSAGE_ENTRY_TYPE;
    (me->content).message.msgid = msgid;
    (me->content).message.from = from;
    me->body = body;
    me->length = length;

    //add it to mailbox queue
    mb->items++;
    MAIL *mail = Calloc(1, sizeof(MAIL));
    mail->me = me;
    mail->next = NULL;
    if (mb->tail == NULL) {
        //first entry in queue
        mb->head = mb->tail = mail;
    } else {
        mb->tail->next = mail;
        mb->tail = mail;
    }

    debug("mailbox (%s) message entry added:", mb->handle);
    debug("Type %d, msgid %d, length %d, body %s", me->type, (me->content).message.msgid, me->length, (char*)me->body);
    debug("mailbox items %d", mb->items);
    V(&mb->items_mutex);
    V(&mb->mutex);
}

/*
 * Add a notice to the end of the mailbox queue.
 *   ntype - the notice type
 *   msgid - the ID of the message to which the notice pertains
 *   body - the body of the notice, which can be arbitrary data, or NULL
 *   length - number of bytes of data in the body
 *
 * The notice body must have been allocated on the heap, but the
 * caller is relieved of the responsibility of ultimately freeing this
 * storage, as it will become the responsibility of whomever removes
 * this notice from the mailbox.
 */
void mb_add_notice(MAILBOX *mb, NOTICE_TYPE ntype, int msgid, void *body, int length) {
    debug("mb_add_notice called..");
    if (mb->defunct) return;
    P(&mb->mutex);
    debug("got mutex");
    if (mb->defunct) {V(&mb->mutex); return;}
    if (mb == NULL) {V(&mb->mutex); return;}
    // debug("allfine");
    MAILBOX_ENTRY *me = Calloc(1, sizeof(MAILBOX_ENTRY));
    me->type = NOTICE_ENTRY_TYPE;
    (me->content).notice.msgid = msgid;
    (me->content).notice.type = ntype;
    me->body = body;
    me->length = length;
    // debug("me created");
    //add it to mailbox queue
    mb->items++;
    MAIL *mail = Calloc(1, sizeof(MAIL));
    mail->me = me;
    mail->next = NULL;
    if (mb->tail == NULL) {
        //first entry in queue
        mb->head = mb->tail = mail;
    } else {
        mb->tail->next = mail;
        mb->tail = mail;
    }
    debug("added to list");
    debug("mailbox (%s) notice entry added:", mb->handle);
    debug("Type %d, msgid %d, length %d, body %s", me->type, (me->content).notice.msgid, me->length, (char*)me->body);
    debug("mailbox items %d", mb->items);

    V(&mb->items_mutex);
    V(&mb->mutex);
}

/*
 * Remove the first entry from the mailbox, blocking until there is
 * one.  The caller assumes the responsibility of freeing the entry
 * and its body, if present.  In addition, if it is a message entry,
 * the caller must decrease the reference count on the "from" mailbox
 * to account for the destruction of the pointer.
 *
 * This function will return NULL in case the mailbox is defunct.
 * The thread servicing the mailbox should use this as an indication
 * that service should be terminated.
 */
//This function free MAIL - internal data struct
MAILBOX_ENTRY *mb_next_entry(MAILBOX *mb) {
    debug("mb_next_entry called");
    if (mb->defunct) return NULL;
    P(&mb->items_mutex); //see if there is entry to remove
    P(&mb->mutex);
    debug("mb_next_entry got access");
    if (mb->defunct) {V(&mb->mutex); return NULL;}

    MAILBOX_ENTRY *me = NULL;
    MAIL *mail = NULL;

    //take entry from the mailbox - dequeue
    if (mb->head == NULL || mb->tail == NULL) debug("head and tail are null during mb_next_entry - fatal error");
    if (mb->head == mb->tail) {
        //Its the only entry in the mailbox
        mail = mb->head;
        mb->head = mb->tail = NULL;
    } else {
        mail = mb->head;
        mb->head = mb->head->next;
    }
    if (mail == NULL)debug("mail is null after moving header - fatal error");
    me = mail->me;
    Free(mail);
    mb->items--;
    debug("mailbox (%s) returning mail:", mb->handle);
    debug("Type %d, length %d, body %s", me->type, me->length, (char*)me->body);
    debug("mailbox items %d", mb->items);
    V(&mb->mutex);
    //This should run after freeing the mutex. Otherwise deadlock.
    if (me->type == MESSAGE_ENTRY_TYPE) {
        // if message entry then reduce the reference.
        mb_unref(me->content.message.from);
    }
    return me;
}