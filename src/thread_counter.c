#include "thread_counter.h"
#include "csapp.h"
#include "debug.h"

struct thread_counter {
    volatile int nums_thread; //current numbers of threads
    sem_t mutex; // mutex to hold threads
    sem_t zmutex; // mutex for wait for zero running thread
};

/*
 * Initialize a new thread counter.
 */
THREAD_COUNTER *tcnt_init(){
    debug("Thread counter init called");
    THREAD_COUNTER *tc = Calloc(1,sizeof(struct thread_counter));
    tc->nums_thread = 0;
    Sem_init(&(tc->mutex),0,1);
    Sem_init(&(tc->zmutex),0,1);
    return tc;
}

/*
 * Finalize a thread counter.
 */
void tcnt_fini(THREAD_COUNTER *tc){
    debug("thread fini called");
    Free(tc);
}

/*
 * Increment a thread counter.
 */
void tcnt_incr(THREAD_COUNTER *tc){
    debug("thread_incr called");
    debug("thread_incr before %d",tc->nums_thread);
    P(&tc->mutex);
    if ( tc->nums_thread == 0) P(&tc->zmutex); //lock access for wait for zero thread count
    (tc->nums_thread)++;
    debug("thread_incr after %d",tc->nums_thread);
    V(&tc->mutex);
}

/*
 * Decrement a thread counter, alerting anybody waiting
 * if the thread count has dropped to zero.
 */
void tcnt_decr(THREAD_COUNTER *tc){
    debug("thread_decr called");
    P(&tc->mutex);
    debug("thread_decr got mutex");
    debug("thread_dec before %d",tc->nums_thread);
    (tc->nums_thread)--;
    if ( tc->nums_thread < 0 ) tc->nums_thread = 0;
    if ( tc->nums_thread == 0) V(&tc->zmutex); //unlock access for wait for zero thread count
    debug("thread_dec after %d",tc->nums_thread);
    V(&tc->mutex);
}

/*
 * A thread calling this function will block in the call until
 * the thread count has reached zero, at which point the
 * function will return.
 */
void tcnt_wait_for_zero(THREAD_COUNTER *tc){
    debug("thread_wait_for_zero called");
    P(&tc->zmutex);
}