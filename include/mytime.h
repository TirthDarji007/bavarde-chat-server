#ifndef MYTIME_H
#define MYTIME_H
#include <arpa/inet.h>

//initializer of the function
void mytime_init();

//Get the time in sec and nsec
int mytime_gettime(uint32_t *time_sec, uint32_t *time_nsec);

#endif