#include "protocol.h"
#include "debug.h"
#include "csapp.h"

/*
 * Send a packet with a specified header and payload.
 *   fd - file descriptor on which packet is to be sent
 *   hdr - the packet header, with multi-byte fields in host byte order
 *   payload - pointer to packet payload, or NULL, if none.
 *
 * Multi-byte fields in the header are converted to network byte order
 * before sending.  The header structure passed to this function may
 * be modified as a result.
 *
 * On success, 0 is returned.
 * On error, -1 is returned and errno is set.
 */
int proto_send_packet(int fd, bvd_packet_header *hdr, void *payload) {
    debug("my proto_send_packet called");
    debug("header data before htonl.\nType %d \tLength %d \tmsgid %d \timestamp_sec %d \timestamp_nsec %d",hdr->type,hdr->payload_length,hdr->msgid,hdr->timestamp_sec,hdr->timestamp_nsec);
    bvd_packet_header *header = Calloc(1,sizeof(bvd_packet_header));
    int bwritten = -1;
    int plength = hdr->payload_length;
    int plength2 = hdr->payload_length;
    header->type = hdr->type;
    header->payload_length = htonl(hdr->payload_length);
    header->msgid = htonl(hdr->msgid);
    header->timestamp_sec = htonl(hdr->timestamp_sec);
    header->timestamp_nsec = htonl(hdr->timestamp_nsec);
    debug("header data after htonl. \nType %d \tLength %d \tmsgid %d \timestamp_sec %d \timestamp_nsec %d",header->type,header->payload_length,header->msgid,header->timestamp_sec,header->timestamp_nsec);

    //write it to network
    bwritten = Rio_writen(fd, header, sizeof(bvd_packet_header));
    Free(header);
    if (bwritten != sizeof(bvd_packet_header))  {debug("write error in proto_send_packet"); return -1;}
    debug("header sent to %d", fd);
    if ( (plength = ! 0) && (payload != NULL) ) {
        //somehow plength gets changed after the comparision. Threfore plength2 variable to for length.
        //also header values are changed after sending the data.
        bwritten =  Rio_writen(fd, payload,plength2);
        debug("payload of %d bytes written.\n%s", bwritten,(char*)payload );
        if(bwritten!=plength2){debug("write error in proto_send_packet"); return -1;}
    }

    return 0;
}

/*
 * Receive a packet, blocking until one is available.
 *  fd - file descriptor from which packet is to be received
 *  hdr - pointer to caller-supplied storage for packet header
 *  payload - variable into which to store payload pointer
 *
 * The returned header has its multi-byte fields in host byte order.
 *
 * If the returned payload pointer is non-NULL, then the caller
 * is responsible for freeing the storage.
 *
 * On success, 0 is returned.
 * On error, -1 is returned, payload and length are left unchanged,
 * and errno is set.
 */
int proto_recv_packet(int fd, bvd_packet_header *hdr, void **payload) {
    debug("my proto_recv_packet called, size of header %lu", sizeof(bvd_packet_header));
    int rbytes = 0;
    int hsize = sizeof(bvd_packet_header);
    rbytes = Rio_readn(fd,hdr,hsize);
    if (rbytes != hsize) return -1;
    hdr->payload_length = ntohl(hdr->payload_length);
    hdr->msgid = ntohl(hdr->msgid);
    hdr->timestamp_sec = ntohl(hdr->timestamp_sec);
    hdr->timestamp_nsec = ntohl(hdr->timestamp_nsec);
    debug("header data after ntohl. \nType %d \tLength %d \tmsgid %d \timestamp_sec %d \timestamp_nsec %d",hdr->type,hdr->payload_length,hdr->msgid,hdr->timestamp_sec,hdr->timestamp_nsec);
    int plength = hdr->payload_length;
    if (plength > 0){
        *payload = Calloc(plength+10,sizeof(char));
        rbytes = Rio_readn(fd,*payload,plength);
        if(rbytes != plength ) return -1;
        if(rbytes == 0) return -1;
        debug("payload received. %s", (char*)*payload);
    }else{
        *payload = NULL;
    }

    return 0;
}