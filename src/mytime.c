#include "mytime.h"
#include "debug.h"
#include "csapp.h"


static sem_t mutex;
struct timespec start, end;
//initializer of the function
void mytime_init(){
    Sem_init(&mutex,0,1);
    clock_gettime(CLOCK_MONOTONIC, &start);
}

//Get the time in sec and nsec
int mytime_gettime(uint32_t *time_sec, uint32_t *time_nsec){
    P(&mutex);
    clock_gettime(CLOCK_MONOTONIC, &end);
    // *time_sec = end.tv_sec - start.tv_sec;
    // *time_nsec = end.tv_nsec - start.tv_nsec;
    *time_sec = start.tv_sec;
    *time_nsec = start.tv_nsec;
    V(&mutex);
    return 0;
}