#include "server.h"
#include "csapp.h"
#include "debug.h"
#include "protocol.h"
#include "directory.h"
#include "mailbox.h"
#include "mytime.h"

/*
 * Thread counter that should be used to track the number of
 * service threads that have been created.
 */
THREAD_COUNTER *thread_counter;

//helpers
void users_list_to_string(char **arg, char **body, int *npsize);
void get_handle_payload(char **payload, char** handle, char** body, int* payload_length);
void send_NACK_notice(int fd, int msgid);
void send_ACK_notice_fd(int fd, int msgid, char *payload, int payload_length);
int send_notice(int fd, NOTICE_TYPE ntype, int payload_length, int msgid, void *payload);
int send_message(int fd, MAILBOX_ENTRY * me);
void append_sender_handle(char *handle, char** body, int *size);
void send_bounce_notice(MAILBOX_ENTRY *me);

/*
 * Thread function for the thread that handles client requests.
 *
 * The arg pointer point to the file descriptor of client connection.
 * This pointer must be freed after the file descriptor has been
 * retrieved.
 */
void *bvd_client_service(void *arg) {
    int *connfd = arg;
    int fd = *connfd; //get the connection fd
    Free(arg); //free the arg space
    debug("bvd_client_service started for fd: %d" , fd);
    tcnt_incr(thread_counter);
    Pthread_detach(Pthread_self());
    char* payload = NULL;
    char *handle = NULL;
    char *body = NULL;
    int npsize = 0; //new payload size
    bvd_packet_header *hdr = Calloc(1, sizeof(bvd_packet_header));
    MAILBOX *mb = NULL;
    MAILBOX *to = NULL;
    pthread_t tid;

    //start receing packet
    while (!proto_recv_packet(fd, hdr, (void**)&payload)) {
        //sucessfully received packet
        if (hdr->type == BVD_LOGIN_PKT) {
            if (payload == NULL) {
                //during login payload cannot be null
                //send NACT notice directly - user not loggedin
                send_NACK_notice(fd, hdr->msgid);
                Free(hdr);
                hdr = Calloc(1, sizeof(bvd_packet_header));
                continue;
            }
            get_handle_payload(&payload, &handle, &body, &npsize);
            mb = dir_register(handle, fd);
            Free(handle); //the handle is copied by mailbox (mb_init). Which is called by dir_register.
            Free(body);
            body = NULL;
            handle = NULL;
            if (mb == NULL) {
                //error in mailbox
                //user may already been logged in or mb is in defunct state.
                //send NACT  directly
                send_NACK_notice(fd, hdr->msgid);
                Free(hdr);
                hdr = Calloc(1, sizeof(bvd_packet_header));
                continue;
            }

            //send ACT notice - via mailbox
            mb_add_notice(mb, ACK_NOTICE_TYPE, hdr->msgid, NULL, 0);
            mb_set_discard_hook(mb, (MAILBOX_DISCARD_HOOK *) &send_bounce_notice);
            //start the mailbox service
            struct fd_and_mb *fdmb = Calloc(1, sizeof(struct fd_and_mb));
            fdmb->fd = fd;
            fdmb->mb = mb;
            mb_ref(mb);
            Pthread_create(&tid, NULL, bvd_mailbox_service, fdmb);
            debug("new thread of mailbox service %lu has been created.", tid);

        } else if (hdr->type == BVD_LOGOUT_PKT) {
            //logout user
            break;
        } else if (hdr->type == BVD_USERS_PKT) {
            //list of users received
            //send ACT notice, send data with this pkt. Need to change this
            users_list_to_string(dir_all_handles(), &body, &npsize);
            if (mb != NULL) {
                mb_add_notice(mb, ACK_NOTICE_TYPE, hdr->msgid, body, npsize);
            } else {
                //is user is not logged in and type "users" - sned directly
                //variable body free by send_ACT_notice_fd
                send_ACK_notice_fd(fd, hdr->msgid, body, npsize );
                //Erorr here:
            }
            npsize = 0;

        } else if (hdr->type == BVD_SEND_PKT) {
            if (payload == NULL)debug("received payload should not be null");
            if (mb == NULL) {
                //sending user is not logged in
                send_NACK_notice(fd, hdr->msgid);
                Free(hdr);
                hdr = Calloc(1, sizeof(bvd_packet_header));
                continue;
            }
            //send message -- add to mailbox of receiver
            get_handle_payload(&payload, &handle, &body, &npsize);
            to = dir_lookup(handle);
            Free(handle);
            if (to == NULL) {
                //send bounce notice b/c receipient has disconnected
                debug("user not found - sending bounce notice");
                mb_add_notice(mb, NACK_NOTICE_TYPE, hdr->msgid, NULL, 0);
                Free(hdr);
                if (body != NULL)Free(body);
                body = NULL;
                hdr = Calloc(1, sizeof(bvd_packet_header));
                continue;
            }
            mb_add_notice(mb, ACK_NOTICE_TYPE, hdr->msgid, NULL, 0);
            append_sender_handle(mb_get_handle(mb), &body, &npsize);
            mb_ref(mb); //adding a message so, counter must be increased.
            if (to != NULL)mb_unref(to); //dir_lookup increase the counter of to.
            mb_add_message(to, hdr->msgid, mb, body, npsize);
            npsize = 0;
        } else {
            //PKT of non-support type
            //send NACT notice
            if (mb == NULL) {
                //NACT directly to sender
                send_NACK_notice(fd, hdr->msgid);
            } else {
                //NACT through mailbox
                mb_add_notice(mb, NACK_NOTICE_TYPE, hdr->msgid, NULL, 0);
            }
        }
        //free old header and get space for new one
        Free(hdr);
        if (payload != NULL)Free(payload);
        hdr = Calloc(1, sizeof(bvd_packet_header));
    }

    //error in receiving packet
    if (hdr != NULL)Free(hdr);
    // if (payload != NULL)Free(payload);
    // if (handle != NULL) Free(handle);
    // if (body != NULL) Free(body);
    if (mb != NULL)mb_unref(mb);
    if (mb != NULL) dir_unregister(mb_get_handle(mb));
    debug("ending bvd_client_service for fd: %d", fd);
    tcnt_decr(thread_counter);
    Close(fd);
    void *p = NULL;
    return p;
}


/*
 * Once the file descriptor and mailbox have been retrieved,
 * this structure must be freed.
 */
void *bvd_mailbox_service(void *arg) {
    debug("bvd_mailbox_service called");
    struct fd_and_mb *fdmb = arg;
    int fd = fdmb->fd;
    MAILBOX *mb = fdmb->mb;
    Free(arg);
    debug("bvd_mailbox_service for fd %d activated", fd);
    tcnt_incr(thread_counter);
    MAILBOX_ENTRY * me = NULL;
    int rval = -1;
    Pthread_detach(Pthread_self());
    // get the mailbox and process data
    while ( (me = mb_next_entry(mb)) != NULL) {

        if (me->type == MESSAGE_ENTRY_TYPE) {
            rval = send_message(fd, me);
            if (rval < 0) {
                //Send bounce notice to message sender
                mb_add_notice(me->content.message.from, BOUNCE_NOTICE_TYPE, me->content.message.msgid, NULL, 0);
            }
            // mb_unref(me->content.message.from);

        } else if (me->type == NOTICE_ENTRY_TYPE ) {
            rval = send_notice(fd, me->content.notice.type, me->length, me->content.notice.msgid, me->body);
            if (rval < 0 ) {
                //error in sending the notice
                debug("cannot able to send notice");
            }
        } else {
            debug("bvd_mailbox_service received unknown MAILBOX_ENTRY");
        }
        rval = -1; //reset the rval
        Free(me);
    }
    debug("ending bvd_mailbox_service for fd: %d", fd);
    tcnt_decr(thread_counter);
    if (mb != NULL)mb_unref(mb);
    void *p = NULL;
    return p;
}

//free the users list
//last is denoted by null
void users_list_to_string(char **arg, char **body, int *npsize) {
    debug("user free list called");
    char **list = arg;
    char *line = NULL;
    int i = 0;
    char *c = NULL;
    char string[2048];
    char *s = string;
    while (list[i] != NULL) {
        line = list[i];
        c = line;
        while (*c != '\0') {
            *s = *c;
            c++;
            s++;
        }
        *s = '\n';
        s++;
        i++;
        Free(line);
    }
    s--;
    *s = '\r';
    s++;
    *s = '\n';
    s++;
    // s--;
    *s = '\0';
    // debug("string computed returned %s", string);
    if ((s - string) > 2048) debug ("buffer overflow at listof users");
    int payload_length = strlen(string) + 1;
    *body = Calloc(payload_length, sizeof(char));
    strcpy(*body, string);
    *npsize = payload_length;
    Free(arg);
    debug("string length %d returned: %s", *npsize, *body);
}

void get_handle_payload(char **payload, char** handle, char** body, int *payload_length) {
    debug("get_handle_payload added");
    char *c = *payload;
    int hsize = 0;
    while (*c++ != '\n')hsize++;
    char *e = c;
    e--;
    e--;
    *e = '\0'; //end the header
    debug("hsize is %d", hsize);
    *handle = Calloc(hsize, sizeof(char));
    // char *s = *handle;
    // while(*s != '\0')s++;
    // *s = '\n';
    // s++;
    // *s='\0';
    strcpy(*handle, *payload);
    // *payload[hsize] = '\0';
    int bsize = strlen(c) + 1;
    *body = calloc(bsize, sizeof(char));
    strcpy(*body, c);
    *payload_length = bsize;
    debug("returning handle payload\nhanle length %d, handle %s\nbody length %d, body %s", hsize, *handle, bsize, *body);
    Free(*payload);
    *payload = NULL;
}

//return NACT without using mailbox
void send_NACK_notice(int fd, int msgid) {
    debug("send nack to fd %d", fd);
    bvd_packet_header *hdr = Calloc(1, sizeof(bvd_packet_header));
    hdr->type = BVD_NACK_PKT;
    hdr->payload_length = 0;
    hdr->msgid = msgid;
    // hdr->timestamp_sec = 0;
    // hdr->timestamp_nsec = 0;
    mytime_gettime(&hdr->timestamp_sec, &hdr->timestamp_nsec);
    proto_send_packet(fd, hdr, NULL);
    Free(hdr);
}

//return NACT without using mailbox, It frees the payload
void send_ACK_notice_fd(int fd, int msgid, char *payload, int payload_length) {
    debug("send nack to fd %d", fd);
    bvd_packet_header *hdr = Calloc(1, sizeof(bvd_packet_header));
    hdr->type = BVD_ACK_PKT;
    hdr->payload_length = payload_length;
    hdr->msgid = msgid;
    mytime_gettime(&hdr->timestamp_sec, &hdr->timestamp_nsec);
    proto_send_packet(fd, hdr, payload);
    Free(hdr);
    Free(payload);
}

//send the notice
//This function fee the payload
int send_notice(int fd, NOTICE_TYPE ntype, int payload_length, int msgid, void *payload) {
    debug("send_notice called");
    bvd_packet_header *hdr = Calloc(1, sizeof(bvd_packet_header));
    if (ntype == ACK_NOTICE_TYPE) {
        hdr->type = BVD_ACK_PKT;
    } else if (ntype == NACK_NOTICE_TYPE) {
        hdr->type = BVD_NACK_PKT;
    } else if (ntype == BOUNCE_NOTICE_TYPE) {
        hdr->type = BVD_BOUNCE_PKT;
    } else if (ntype == RRCPT_NOTICE_TYPE) {
        hdr->type = BVD_RRCPT_PKT;
    } else {
        debug("some unknown notice is being send");
    }

    hdr->payload_length = payload_length;
    hdr->msgid = msgid;
    // hdr->timestamp_sec = time_s;
    // hdr->timestamp_nsec = time_ns;
    mytime_gettime(&hdr->timestamp_sec, &hdr->timestamp_nsec);
    int rval = proto_send_packet(fd, hdr, payload);
    Free(hdr);
    if (payload != NULL) Free(payload);
    return rval;
}

int send_message(int fd, MAILBOX_ENTRY * me) {
    debug("send_message called");
    bvd_packet_header *hdr = Calloc(1, sizeof(bvd_packet_header));
    hdr->type = BVD_DLVR_PKT;
    hdr->payload_length = me->length;
    hdr->msgid = me->content.message.msgid;
    int mid = me->content.message.msgid;
    // hdr->timestamp_sec = 0;
    // hdr->timestamp_nsec = 0;
    mytime_gettime(&hdr->timestamp_sec, &hdr->timestamp_nsec);
    int rval = proto_send_packet(fd, hdr, me->body);
    if (me->body != NULL) Free(me->body);
    if ( rval == 0) {
        //sucessfully message sent. send rrspt to sender
        mb_add_notice(me->content.message.from, RRCPT_NOTICE_TYPE, mid, NULL, 0);
    } else {
        //error in sending message
        //send bounce notice to sender
        mb_add_notice(me->content.message.from, BOUNCE_NOTICE_TYPE, mid, NULL, 0);
    }

    Free(hdr);
    return rval;
}

//This function appends the hanle to given body of size
void append_sender_handle(char *handle, char** body, int *size) {
    debug("append_sender_handle called");
    int hsize = strlen(handle) + 2; // 2 for \r\n
    int bsize = strlen(*body) + 3; // 1 for \0
    int nsize = hsize + bsize;
    char *payload = Calloc(nsize, sizeof(char));
    char *p = payload;
    char *b = *body;
    char *h = handle;

    while (*h != '\0') {
        *p = *h;
        p++;
        h++;
    }
    *p++ = '\r';
    *p++ = '\n';
    while (*b != '\0') {
        *p = *b;
        p++;
        b++;
    }
    //uncommet below to add \r\n at the end of the payload
    // *p = '\r';
    // p++;
    // *p = '\n';
    // p++;
    *p = '\0';
    Free(*body);
    *body = payload;
    *size = nsize;
    debug("new %d len payload\n %s", *size, *body);
}

//This function sends the bounce notice to all mail from that mailbox
void send_bounce_notice(MAILBOX_ENTRY *me) {
    debug("send bounce notice called");
    if (me->type == MESSAGE_ENTRY_TYPE) {
        mb_add_notice((me->content).message.from, BOUNCE_NOTICE_TYPE, 0, NULL, 0);
    }
}